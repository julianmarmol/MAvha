﻿Imports ClassLibrary1.Business

Public Class Form1

    Private Sub BotonAgregar_Click(sender As Object, e As EventArgs) Handles BotonAgregar.Click

        Dim nombre As String
        Dim fecha As String
        Dim edad As String
        Dim sexo As String

        nombre = TextNombre.Text
        fecha = TextFecha.Text
        edad = TextEdad.Text
        sexo = ComboSexo.Text

        AgregarPersona(nombre, fecha, edad, sexo)

        'Actualizar Grilla
        Me.PersonasTableAdapter.Fill(Me.DatosDataSet.personas)

        MessageBox.Show("Persona Agregada Exitosamente")

    End Sub



    Private Sub BotonActualizar_Click(sender As Object, e As EventArgs) Handles BotonActualizar.Click

        Dim nombre As String
        Dim fecha As String
        Dim edad As String
        Dim sexo As String

        nombre = DataGridView1.CurrentRow.Cells(1).Value
        fecha = DataGridView1.CurrentRow.Cells(2).Value
        edad = DataGridView1.CurrentRow.Cells(3).Value
        sexo = DataGridView1.CurrentRow.Cells(4).Value

        EditarPersona(DataGridView1.CurrentRow.Cells(0).Value, nombre, fecha, edad, sexo)
        MessageBox.Show("Persona Actualizada Exitosamente")

    End Sub

    Private Sub BotonBorrar_Click(sender As Object, e As EventArgs) Handles BotonBorrar.Click

        Dim id As Guid
        id = DataGridView1.CurrentRow.Cells(0).Value

        EliminarPersona(DataGridView1.CurrentRow.Cells(0).Value)

        'Actualizar Grilla
        Me.PersonasTableAdapter.Fill(Me.DatosDataSet.personas)

        MessageBox.Show("Persona Eliminada Exitosamente")

    End Sub


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.PersonasTableAdapter.Fill(Me.DatosDataSet.personas)

    End Sub

    Private Sub FillByToolStripButton_Click(sender As Object, e As EventArgs)
        Try
            Me.PersonasTableAdapter.FillBy(Me.DatosDataSet.personas)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FillByActivoToolStripButton_Click(sender As Object, e As EventArgs)
        Try
            Me.PersonasTableAdapter.FillByActivo(Me.DatosDataSet.personas)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FillByActivoToolStripButton1_Click(sender As Object, e As EventArgs)
        Try
            Me.PersonasTableAdapter.FillByActivo(Me.DatosDataSet.personas)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FillByActivoToolStripButton_Click_1(sender As Object, e As EventArgs) Handles FillByActivoToolStripButton.Click
        Try
            Me.PersonasTableAdapter.FillByActivo(Me.DatosDataSet.personas)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Mostrar_TodosToolStripButton_Click(sender As Object, e As EventArgs) Handles Mostrar_TodosToolStripButton.Click
        Try
            Me.PersonasTableAdapter.Mostrar_Todos(Me.DatosDataSet.personas)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


End Class
