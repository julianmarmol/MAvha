﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BotonAgregar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ComboSexo = New System.Windows.Forms.ComboBox()
        Me.TextNombre = New System.Windows.Forms.TextBox()
        Me.TextFecha = New System.Windows.Forms.TextBox()
        Me.TextEdad = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EdadDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SexoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ActivoDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.PersonasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatosDataSet = New WinFormABMPersonas.datosDataSet()
        Me.BotonActualizar = New System.Windows.Forms.Button()
        Me.BotonBorrar = New System.Windows.Forms.Button()
        Me.PersonasTableAdapter = New WinFormABMPersonas.datosDataSetTableAdapters.personasTableAdapter()
        Me.FillByActivoToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.FillByActivoToolStrip = New System.Windows.Forms.ToolStrip()
        Me.FillByActivoToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.Mostrar_TodosToolStrip = New System.Windows.Forms.ToolStrip()
        Me.Mostrar_TodosToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.Label6 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PersonasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FillByActivoToolStrip.SuspendLayout()
        Me.Mostrar_TodosToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'BotonAgregar
        '
        Me.BotonAgregar.Location = New System.Drawing.Point(499, 136)
        Me.BotonAgregar.Name = "BotonAgregar"
        Me.BotonAgregar.Size = New System.Drawing.Size(127, 47)
        Me.BotonAgregar.TabIndex = 0
        Me.BotonAgregar.Text = "Agregar"
        Me.BotonAgregar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(196, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(521, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "ABM PERSONA - Para editar hacerlo en la misma grilla y luego click en actualizar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(98, 95)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 17)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Nombre"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(98, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(141, 17)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Fecha de Nacimiento"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(99, 178)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 17)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Edad"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(101, 223)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 17)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Sexo"
        '
        'ComboSexo
        '
        Me.ComboSexo.FormattingEnabled = True
        Me.ComboSexo.Items.AddRange(New Object() {"Masculino", "Femenino"})
        Me.ComboSexo.Location = New System.Drawing.Point(270, 223)
        Me.ComboSexo.Name = "ComboSexo"
        Me.ComboSexo.Size = New System.Drawing.Size(172, 24)
        Me.ComboSexo.TabIndex = 7
        '
        'TextNombre
        '
        Me.TextNombre.Location = New System.Drawing.Point(270, 95)
        Me.TextNombre.Name = "TextNombre"
        Me.TextNombre.Size = New System.Drawing.Size(172, 22)
        Me.TextNombre.TabIndex = 8
        '
        'TextFecha
        '
        Me.TextFecha.Location = New System.Drawing.Point(270, 136)
        Me.TextFecha.Name = "TextFecha"
        Me.TextFecha.Size = New System.Drawing.Size(172, 22)
        Me.TextFecha.TabIndex = 9
        '
        'TextEdad
        '
        Me.TextEdad.Location = New System.Drawing.Point(270, 178)
        Me.TextEdad.Name = "TextEdad"
        Me.TextEdad.Size = New System.Drawing.Size(172, 22)
        Me.TextEdad.TabIndex = 10
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDataGridViewTextBoxColumn, Me.NombreDataGridViewTextBoxColumn, Me.FechaDataGridViewTextBoxColumn, Me.EdadDataGridViewTextBoxColumn, Me.SexoDataGridViewTextBoxColumn, Me.ActivoDataGridViewCheckBoxColumn})
        Me.DataGridView1.DataSource = Me.PersonasBindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(101, 298)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.Size = New System.Drawing.Size(855, 169)
        Me.DataGridView1.TabIndex = 11
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "id"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        Me.IdDataGridViewTextBoxColumn.Visible = False
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "nombre"
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "nombre"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        '
        'FechaDataGridViewTextBoxColumn
        '
        Me.FechaDataGridViewTextBoxColumn.DataPropertyName = "fecha"
        Me.FechaDataGridViewTextBoxColumn.HeaderText = "fecha"
        Me.FechaDataGridViewTextBoxColumn.Name = "FechaDataGridViewTextBoxColumn"
        '
        'EdadDataGridViewTextBoxColumn
        '
        Me.EdadDataGridViewTextBoxColumn.DataPropertyName = "edad"
        Me.EdadDataGridViewTextBoxColumn.HeaderText = "edad"
        Me.EdadDataGridViewTextBoxColumn.Name = "EdadDataGridViewTextBoxColumn"
        '
        'SexoDataGridViewTextBoxColumn
        '
        Me.SexoDataGridViewTextBoxColumn.DataPropertyName = "sexo"
        Me.SexoDataGridViewTextBoxColumn.HeaderText = "sexo"
        Me.SexoDataGridViewTextBoxColumn.Name = "SexoDataGridViewTextBoxColumn"
        '
        'ActivoDataGridViewCheckBoxColumn
        '
        Me.ActivoDataGridViewCheckBoxColumn.DataPropertyName = "activo"
        Me.ActivoDataGridViewCheckBoxColumn.HeaderText = "activo"
        Me.ActivoDataGridViewCheckBoxColumn.Name = "ActivoDataGridViewCheckBoxColumn"
        Me.ActivoDataGridViewCheckBoxColumn.Visible = False
        '
        'PersonasBindingSource
        '
        Me.PersonasBindingSource.DataMember = "personas"
        Me.PersonasBindingSource.DataSource = Me.DatosDataSet
        '
        'DatosDataSet
        '
        Me.DatosDataSet.DataSetName = "datosDataSet"
        Me.DatosDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BotonActualizar
        '
        Me.BotonActualizar.Location = New System.Drawing.Point(499, 497)
        Me.BotonActualizar.Name = "BotonActualizar"
        Me.BotonActualizar.Size = New System.Drawing.Size(133, 55)
        Me.BotonActualizar.TabIndex = 12
        Me.BotonActualizar.Text = "Actualizar"
        Me.BotonActualizar.UseVisualStyleBackColor = True
        '
        'BotonBorrar
        '
        Me.BotonBorrar.Location = New System.Drawing.Point(653, 497)
        Me.BotonBorrar.Name = "BotonBorrar"
        Me.BotonBorrar.Size = New System.Drawing.Size(131, 55)
        Me.BotonBorrar.TabIndex = 13
        Me.BotonBorrar.Text = "Borrar"
        Me.BotonBorrar.UseVisualStyleBackColor = True
        '
        'PersonasTableAdapter
        '
        Me.PersonasTableAdapter.ClearBeforeFill = True
        '
        'FillByActivoToolStrip1
        '
        Me.FillByActivoToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.FillByActivoToolStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.FillByActivoToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.FillByActivoToolStrip1.Name = "FillByActivoToolStrip1"
        Me.FillByActivoToolStrip1.Size = New System.Drawing.Size(1127, 25)
        Me.FillByActivoToolStrip1.TabIndex = 16
        Me.FillByActivoToolStrip1.Text = "FillByActivoToolStrip1"
        '
        'FillByActivoToolStrip
        '
        Me.FillByActivoToolStrip.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.FillByActivoToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FillByActivoToolStripButton})
        Me.FillByActivoToolStrip.Location = New System.Drawing.Point(0, 25)
        Me.FillByActivoToolStrip.Name = "FillByActivoToolStrip"
        Me.FillByActivoToolStrip.Size = New System.Drawing.Size(1127, 27)
        Me.FillByActivoToolStrip.TabIndex = 17
        Me.FillByActivoToolStrip.Text = "FillByActivoToolStrip"
        '
        'FillByActivoToolStripButton
        '
        Me.FillByActivoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.FillByActivoToolStripButton.Name = "FillByActivoToolStripButton"
        Me.FillByActivoToolStripButton.Size = New System.Drawing.Size(90, 24)
        Me.FillByActivoToolStripButton.Text = "FillByActivo"
        '
        'Mostrar_TodosToolStrip
        '
        Me.Mostrar_TodosToolStrip.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.Mostrar_TodosToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Mostrar_TodosToolStripButton})
        Me.Mostrar_TodosToolStrip.Location = New System.Drawing.Point(0, 52)
        Me.Mostrar_TodosToolStrip.Name = "Mostrar_TodosToolStrip"
        Me.Mostrar_TodosToolStrip.Size = New System.Drawing.Size(1127, 27)
        Me.Mostrar_TodosToolStrip.TabIndex = 18
        Me.Mostrar_TodosToolStrip.Text = "Mostrar_TodosToolStrip"
        '
        'Mostrar_TodosToolStripButton
        '
        Me.Mostrar_TodosToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.Mostrar_TodosToolStripButton.Name = "Mostrar_TodosToolStripButton"
        Me.Mostrar_TodosToolStripButton.Size = New System.Drawing.Size(110, 24)
        Me.Mostrar_TodosToolStripButton.Text = "Mostrar_Todos"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Bisque
        Me.Label6.Location = New System.Drawing.Point(496, 79)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(579, 17)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "ABM DE PERSONAS. Para actualiar hacerlo INLINE sobre la grilla y luego click en e" &
    "l botón"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1127, 657)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Mostrar_TodosToolStrip)
        Me.Controls.Add(Me.FillByActivoToolStrip)
        Me.Controls.Add(Me.FillByActivoToolStrip1)
        Me.Controls.Add(Me.BotonBorrar)
        Me.Controls.Add(Me.BotonActualizar)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.TextEdad)
        Me.Controls.Add(Me.TextFecha)
        Me.Controls.Add(Me.TextNombre)
        Me.Controls.Add(Me.ComboSexo)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BotonAgregar)
        Me.Name = "Form1"
        Me.Text = "Formulario"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PersonasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FillByActivoToolStrip.ResumeLayout(False)
        Me.FillByActivoToolStrip.PerformLayout()
        Me.Mostrar_TodosToolStrip.ResumeLayout(False)
        Me.Mostrar_TodosToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BotonAgregar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents ComboSexo As ComboBox
    Friend WithEvents TextNombre As TextBox
    Friend WithEvents TextFecha As TextBox
    Friend WithEvents TextEdad As TextBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents DatosDataSet As datosDataSet
    Friend WithEvents PersonasBindingSource As BindingSource
    Friend WithEvents PersonasTableAdapter As datosDataSetTableAdapters.personasTableAdapter
    Friend WithEvents BotonActualizar As Button
    Friend WithEvents BotonBorrar As Button
    Friend WithEvents FillByActivoToolStrip1 As ToolStrip
    Friend WithEvents FillByActivoToolStrip As ToolStrip
    Friend WithEvents FillByActivoToolStripButton As ToolStripButton
    Friend WithEvents Mostrar_TodosToolStrip As ToolStrip
    Friend WithEvents Mostrar_TodosToolStripButton As ToolStripButton
    Friend WithEvents IdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NombreDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FechaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents EdadDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SexoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ActivoDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents Label6 As Label
End Class
