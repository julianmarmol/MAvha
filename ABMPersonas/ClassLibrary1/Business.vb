﻿

Public Class Business


    ''' <summary>
    ''' Agregar Persona : dado Nombre , Nacimiento , Edad y Sexo, damos de alta una nueva entidad
    ''' </summary>
    ''' <param name="nombre"></param>
    ''' <param name="nacimiento"></param>
    ''' <param name="edad"></param>
    ''' <param name="sexo"></param>
    Public Shared Sub AgregarPersona(ByVal nombre As String, ByVal nacimiento As String, ByVal edad As String, ByVal sexo As String)

        Dim Servicio As New ServiceReferencePersonas.PersonasSoapClient()
        Dim p As Boolean

        p = Servicio.AgregarPersonaService(nombre, nacimiento, edad, sexo)


    End Sub


    ''' <summary>
    ''' Editar Persona : dado Nombre , Nacimiento , Edad y Sexo, actualizamos la entidad
    ''' </summary>
    ''' <param name="id"></param>
    ''' <param name="nombre"></param>
    ''' <param name="nacimiento"></param>
    ''' <param name="edad"></param>
    ''' <param name="sexo"></param>
    Public Shared Sub EditarPersona(ByVal id As Guid, ByVal nombre As String, ByVal nacimiento As String, ByVal edad As String, ByVal sexo As String)

        Dim Servicio As New ServiceReferencePersonas.PersonasSoapClient()
        Dim p As Boolean

        p = Servicio.EditarPersonaService(id, nombre, nacimiento, edad, sexo)


    End Sub

    ''' <summary>
    ''' Eliminar Persona : dado Id se borra solo de forma lógica cambiando su estado
    ''' </summary>
    ''' <param name="id"></param>

    Public Shared Sub EliminarPersona(ByVal id As Guid)

        Dim Servicio As New ServiceReferencePersonas.PersonasSoapClient()
        Dim p As Boolean

        p = Servicio.EliminarPersonaService(id)


    End Sub


End Class
