﻿Imports System.Web.Services
Imports System.ComponentModel
Imports Servidor.Conexion
Imports System.Data.SqlClient


<System.Web.Services.WebService(Namespace:="http://localhost/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class Personas
    Inherits System.Web.Services.WebService

    Const Cadena As String = "Data Source=.\SQLEXPRESS;Database=datos;Integrated Security=SSPI"

    Public Shared dst As DataSet

    <WebMethod()> Public Function AgregarPersonaService(ByVal nombre As String, ByVal fecha As String, ByVal edad As String, ByVal sexo As String)

        Dim query As String = String.Empty
        query &= "INSERT INTO personas (id, nombre, fecha, edad, "
        query &= "sexo, activo)"
        query &= "VALUES (@id, @nombre,@fecha, @edad, @sexo,@activo)"

        Dim conn = New SqlConnection(Cadena)


        Using comm As New SqlCommand()
            With comm
                .Connection = conn
                .CommandType = CommandType.Text
                .CommandText = query
                .Parameters.AddWithValue("@id", Guid.NewGuid())
                .Parameters.AddWithValue("@nombre", nombre)
                .Parameters.AddWithValue("@fecha", fecha)
                .Parameters.AddWithValue("@edad", edad)
                .Parameters.AddWithValue("@sexo", sexo)
                .Parameters.AddWithValue("@activo", True)

            End With

            Try

                conn.Open()
                comm.ExecuteNonQuery()

            Catch ex As Exception

            End Try


        End Using

        Return True

    End Function

    <WebMethod()> Public Function EditarPersonaService(ByVal id As Guid, nombre As String, ByVal fecha As String, ByVal edad As String, ByVal sexo As String)

        Dim query As String = String.Empty
        query &= "UPDATE personas SET nombre = @nombre , fecha = @fecha , edad  = @edad , sexo = @sexo WHERE id  = @id"

        Dim conn = New SqlConnection(Cadena)


        Using comm As New SqlCommand()
            With comm
                .Connection = conn
                .CommandType = CommandType.Text
                .CommandText = query
                .Parameters.AddWithValue("@id", id)
                .Parameters.AddWithValue("@nombre", nombre)
                .Parameters.AddWithValue("@fecha", fecha)
                .Parameters.AddWithValue("@edad", edad)
                .Parameters.AddWithValue("@sexo", sexo)


            End With

            Try

                conn.Open()
                comm.ExecuteNonQuery()

            Catch ex As Exception

            End Try


        End Using

        Return True

    End Function

    <WebMethod()> Public Function EliminarPersonaService(ByVal id As Guid)

        Dim query As String = String.Empty
        query &= "UPDATE personas SET activo = 0 WHERE id = @id"


        Dim conn = New SqlConnection(Cadena)


        Using comm As New SqlCommand()
            With comm
                .Connection = conn
                .CommandType = CommandType.Text
                .CommandText = query
                .Parameters.AddWithValue("@id", id)

            End With

            Try

                conn.Open()
                comm.ExecuteNonQuery()

            Catch ex As Exception

            End Try


        End Using

        Return True

    End Function


End Class